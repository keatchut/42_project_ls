/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:41:50 by keatchut          #+#    #+#             */
/*   Updated: 2018/01/31 22:11:02 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	char	illegaloption(char *s)
{
	int		a;
	int		b;
	char	usage[11];

	a = 0;
	b = 0;
	ft_strcpy(usage, "GRSaglortu");
	while (usage[b] != '\0')
	{
		if (s[a] == usage[b])
		{
			a++;
			b = -1;
		}
		b++;
	}
	if (s[a] != '\0')
		return (s[a]);
	return (0);
}

static	void	fill_opt(char *s, t_opt *opt, int i)
{
	while (s[i] != '\0')
	{
		if (s[i] == 'g')
			opt->g = 1;
		if (s[i] == 'o')
			opt->o = 1;
		if (s[i] == 'l' || s[i] == 'g' || s[i] == 'o')
			opt->l = 1;
		else if (s[i] == 'r')
			opt->r = 1;
		else if (s[i] == 'S')
			opt->ss = 1;
		else if (s[i] == 'a')
			opt->a = 1;
		else if (s[i] == 'R')
			opt->rr = 1;
		else if (s[i] == 't')
			opt->t = 1;
		else if (s[i] == 'G')
			opt->gg = 1;
		else if (s[i] == 'u')
			opt->u = 1;
		i++;
	}
}

void			get_opt(char **av, t_opt *opt, int *i, int c)
{
	while (av[*i])
	{
		if ((c == 0 && av[*i][0] != '-')
				|| (c == 0 && av[*i][1] == '-' && !av[*i][2]))
		{
			*i += (av[*i][1] == '-' && av[*i][0] == '-') ? 1 : 0;
			return ;
		}
		if (c == 0 && av[*i][0] == '-')
		{
			if (!av[*i][1])
				return ;
			else
			{
				if ((opt->invalid = illegaloption(av[*i] + 1)) != 0)
					return ;
				fill_opt(av[*i] + 1, opt, 0);
			}
		}
		(*i)++;
	}
}

static	int		cased(t_btree **treef, t_btree **treed, char *path, t_opt *opt)
{
	struct stat	s;

	if (stat(path, &s) == 0)
	{
		if (S_ISDIR(s.st_mode))
		{
			insert_data(treed, path, opt, path);
			opt->rec++;
		}
		else
			insert_data(treef, path, opt, path);
		return (1);
	}
	return (0);
}

t_btree			*cof_tree(char *av, int f, t_opt *opt)
{
	static t_btree	*error;
	static t_btree	*d;
	static t_btree	*o;
	struct stat		s;
	t_btree			**tmp;

	if (f == 1 || (av && (lstat(av, &s) != 0)))
		tmp = &error;
	else if (f == 3 || (av && S_ISDIR(s.st_mode)))
		tmp = &d;
	else if (av && S_ISLNK(s.st_mode))
	{
		if (cased(&o, &d, av, opt))
			return (0);
	}
	else
		tmp = &o;
	if (av)
	{
		opt->rec++;
		insert_data(tmp, av, opt, av);
	}
	else
		return (*tmp);
	return (0);
}
