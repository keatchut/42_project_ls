/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:29:48 by keatchut          #+#    #+#             */
/*   Updated: 2018/02/01 02:33:47 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft/libft.h"
# include <sys/types.h>
# include <sys/dir.h>
# include <dirent.h>
# include <stdio.h>
# include <sys/stat.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <sys/xattr.h>
# include <time.h>
# include <sys/errno.h>
# include <limits.h>
# include <sys/acl.h>

typedef struct	s_max
{
	int			lnk;
	int			usr;
	int			grp;
	int			size;
}				t_max;

typedef struct	s_opt
{
	int		l;
	int		r;
	int		a;
	int		rr;
	int		ss;
	int		t;
	int		gg;
	int		u;
	int		g;
	int		o;
	int		rec;
	char	invalid;
	t_max	*len;
}				t_opt;

typedef struct	s_btree
{
	struct s_btree	*right;
	struct s_btree	*left;
	char			name[NAME_MAX + 1];
	char			path[PATH_MAX];
	mode_t			mode;
	nlink_t			nlinks;
	char			*group;
	char			*usr;
	dev_t			dev;
	time_t			m_time;
	time_t			a_time;
	off_t			size;
	char			attr;
	int				bs;
	int				stat_check;
}				t_btree;

void			find(t_btree *root, int f, t_opt *opt);
void			apply_infix(t_btree *root, t_opt *opt);
void			apply_outfix(t_btree *root, t_opt *opt);
int				sortbyatime(const char *av1, const char *av2);
int				sortbymtime(const char *av1, const char *av2);
int				sortbysize(const char *av1, const char *av2);
long long		insert_data(t_btree **root, char *path, t_opt *opt, char *name);
void			filllinks(char *s, int i, nlink_t nlinks, int a);
void			fillgroup(char *s, int i, t_btree *tree, t_opt *opt);
void			fillsize(char *s, int i, t_btree *tree, int a);
void			filldate(char *s, int i, t_btree *tree, t_opt *opt);
void			fillname(char *s, t_btree *tree, t_opt *opt);
void			get_opt(char **av, t_opt *opt, int *i, int c);
t_btree			*cof_tree(char *av, int f, t_opt *opt);
void			build_lformat(t_btree *tree, t_opt *opt, int i, int a);
void			find_fold_onl(char *name, t_opt *opt);
void			find_fold_elem(char *name, t_opt *opt);
void			total_block(int total);
void			errno_message(char *name);
char			*colorname(t_btree *tree);
int				fillsize2(char *s, int i, t_btree *tree);
void			filldate2(char *s, int i, int a, char *date);
void			fill_len(t_btree *tree, t_max *len);

#endif
