/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:39:21 by keatchut          #+#    #+#             */
/*   Updated: 2018/01/31 03:42:54 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		fillsize2(char *s, int i, t_btree *tree)
{
	int min;
	int max;
	int	t;

	t = i;
	min = minor(tree->dev);
	max = major(tree->dev);
	while (min > 9)
	{
		s[i] = (min % 10) + '0';
		min = min / 10;
		i--;
	}
	s[i--] = min + '0';
	while (i > t - 4)
		s[i--] = ' ';
	s[i--] = ',';
	while (max > 9)
	{
		s[i] = (max % 10) + '0';
		max = max / 10;
		i--;
	}
	s[i--] = max + '0';
	return (i);
}

void	filldate2(char *s, int i, int a, char *date)
{
	while (a < 11)
		s[i++] = date[a++];
	a = 19;
	while (a < 24)
		s[i++] = date[a++];
	s[i++] = ' ';
	s[i++] = '\0';
}
