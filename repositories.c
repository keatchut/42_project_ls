/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repositories.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:42:00 by keatchut          #+#    #+#             */
/*   Updated: 2018/02/07 00:03:08 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	int		get_type(char path[PATH_MAX])
{
	struct stat	stt;

	if (lstat(path, &stt) != 0)
		return (0);
	if (S_ISDIR(stt.st_mode))
		return (4);
	return (0);
}

static void		get_path(char path[PATH_MAX], char *c_dir, char *n_name)
{
	int i;
	int y;

	i = 0;
	y = 0;
	if (!c_dir[1] && c_dir[0] == '/')
		i = 1;
	while (c_dir[i] != '\0' && y < PATH_MAX)
		path[y++] = c_dir[i++];
	i = 0;
	path[y++] = '/';
	while (n_name[i] != '\0' && y < PATH_MAX)
	{
		path[y] = n_name[i];
		i++;
		y++;
	}
	path[y] = '\0';
}

void			find_fold_onl(char *name, t_opt *opt)
{
	DIR					*fold;
	struct dirent		*dir;
	char				path[PATH_MAX];
	t_btree				*root;

	root = NULL;
	fold = opendir(name);
	if (!fold)
		return ;
	while ((dir = readdir(fold)) != NULL)
	{
		get_path(path, name, dir->d_name);
		if (get_type(path) == 4 && (dir->d_name[0] != '.' ||
			(opt->a == 1 && dir->d_name[1] != '.' && dir->d_name[1] != '/' &&
			dir->d_name[1] != '\0')))
			insert_data(&root, path, opt, dir->d_name);
	}
	if (fold)
		closedir(fold);
	find(root, 3, opt);
}

static	void	readfold(DIR *fold, char *name, t_opt *opt, t_btree **root)
{
	char			path[PATH_MAX];
	struct dirent	*dir;
	long long		i;

	i = 0;
	while ((dir = readdir(fold)) != NULL)
	{
		if ((opt->a == 1) || (dir->d_name[0] != '.'))
		{
			get_path(path, name, dir->d_name);
			i += insert_data(root, path, opt, dir->d_name);
		}
	}
	if (*root)
	{
		if (opt->l == 1 && (*root)->stat_check == 1)
			total_block(i);
	}
}

void			find_fold_elem(char *name, t_opt *opt)
{
	DIR						*fold;
	t_btree					*root;
	t_max					*len;

	len = opt->len;
	root = NULL;
	errno = 0;
	len->lnk = 0;
	len->usr = 0;
	len->grp = 0;
	len->size = 0;
	opt->len = len;
	fold = opendir(name);
	if (!fold)
	{
		if (name[0] == '.' || name[0] == '/')
			errno_message(ft_strrchr(name, '/') + 1);
		else
			errno_message(name);
		return ;
	}
	readfold(fold, name, opt, &root);
	(opt->r == 0) ? apply_infix(root, opt)
		: apply_outfix(root, opt);
	if (fold)
		closedir(fold);
}
