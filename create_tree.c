/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_tree.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:35:56 by keatchut          #+#    #+#             */
/*   Updated: 2018/01/31 03:39:03 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	int		long_info(t_btree *tree, char *path, long long *bl)
{
	struct stat		s;
	struct passwd	*info;
	struct group	*info2;

	if (lstat(path, &s) != 0)
		return (0);
	if (!(info = getpwuid(s.st_uid))
			|| !(info2 = getgrgid(s.st_gid)))
		tree->stat_check = 0;
	else
	{
		tree->stat_check = 1;
		tree->group = info2->gr_name;
		tree->usr = info->pw_name;
	}
	tree->mode = s.st_mode;
	tree->nlinks = s.st_nlink;
	tree->dev = s.st_rdev;
	tree->size = s.st_size;
	tree->m_time = s.st_mtimespec.tv_sec;
	tree->a_time = s.st_atimespec.tv_sec;
	tree->bs = s.st_blocks;
	*bl = s.st_blocks;
	return (1);
}

static	t_btree	*create_node(char *path, char *name, long long *bl, t_opt *opt)
{
	t_btree	*tree;

	if (!(tree = (t_btree *)malloc(sizeof(t_btree))))
		return (0);
	if (!long_info(tree, path, bl))
		tree->stat_check = 0;
	ft_strcpy(tree->path, path);
	ft_strcpy(tree->name, name);
	tree->left = 0;
	tree->right = 0;
	if (tree->stat_check == 1)
		fill_len(tree, opt->len);
	return (tree);
}

void			choosesort(int (**cmpf)(const char *, const char *),
		t_opt *opt)
{
	if (opt->t == 0 && opt->ss == 0)
		*cmpf = &ft_strcmp;
	else if (opt->ss == 0 && opt->t == 1 && opt->u == 0)
		*cmpf = &sortbymtime;
	else if (opt->ss == 0 && opt->u == 1 && opt->t == 1)
		*cmpf = &sortbyatime;
	else
		*cmpf = &sortbysize;
}

long long		insert_data(t_btree **root, char *path,
		t_opt *opt, char *name)
{
	t_btree		*tree;
	long long	bl;
	int			(*cmpf)(const char *, const char *);

	choosesort(&cmpf, opt);
	tree = *root;
	bl = 0;
	if (tree == 0)
	{
		tree = create_node(path, name, &bl, opt);
		*root = tree;
	}
	else
	{
		if (cmpf(tree->path, path) > 0)
			bl = insert_data(&tree->left, path, opt, name);
		else
			bl = insert_data(&tree->right, path, opt, name);
	}
	return (bl);
}
