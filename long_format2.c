/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_format2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:41:26 by keatchut          #+#    #+#             */
/*   Updated: 2018/02/05 04:53:55 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	filllinks(char *s, int i, nlink_t nlinks, int a)
{
	int n;

	n = nlinks;
	if (n == 0)
		n = 1;
	s[i + 1] = ' ';
	while (n > 9)
	{
		s[i] = (n % 10) + '0';
		n = n / 10;
		i--;
	}
	s[i--] = n + '0';
	while (i >= a)
		s[i--] = ' ';
}

void	fillgroup(char *s, int i, t_btree *tree, t_opt *opt)
{
	int		a;
	int		b;
	t_max	*len;

	len = opt->len;
	a = 0;
	b = i + len->usr;
	if (opt->g == 0)
	{
		while (tree->usr[a] != '\0')
			s[i++] = tree->usr[a++];
		while (i < b + 2)
			s[i++] = ' ';
		a = 0;
	}
	if (opt->o == 0)
	{
		while (tree->group[a] != '\0')
			s[i++] = tree->group[a++];
		while (i < b + len->grp + 2)
			s[i++] = ' ';
	}
}

void	fillsize(char *s, int i, t_btree *tree, int a)
{
	long	sz;

	sz = tree->size;
	if (S_ISCHR(tree->mode) || S_ISBLK(tree->mode))
		i = fillsize2(s, i, tree);
	else
	{
		while (sz > 9)
		{
			s[i] = (sz % 10) + '0';
			sz = sz / 10;
			i--;
		}
		s[i--] = sz + '0';
	}
	while (i >= a)
		s[i--] = ' ';
}

void	filldate(char *s, int i, t_btree *tree, t_opt *opt)
{
	char	*date;
	time_t	actual;
	time_t	t;
	int		a;

	a = 4;
	if (opt->u == 1)
		t = tree->a_time;
	else
		t = tree->m_time;
	date = ctime(&t);
	actual = time(0);
	s[i - 1] = ' ';
	if (actual - t < 15770000)
	{
		while (a < 16)
			s[i++] = date[a++];
		s[i++] = ' ';
		s[i++] = '\0';
	}
	else
		filldate2(s, i, a, date);
}

void	fillname(char *s, t_btree *tree, t_opt *opt)
{
	char	link[PATH_MAX];
	int		a;
	int		i;

	a = 0;
	i = 0;
	if (opt->gg == 1)
		ft_strcpy(tree->name, colorname(tree));
	while (tree->name[a] != '\0')
		s[i++] = tree->name[a++];
	a = 0;
	if (S_ISLNK(tree->mode))
	{
		ft_bzero((void *)link, PATH_MAX);
		readlink(tree->path, link, PATH_MAX);
		s[i++] = ' ';
		s[i++] = '-';
		s[i++] = '>';
		s[i++] = ' ';
		while (link[a] != '\0')
			s[i++] = link[a++];
	}
	s[i++] = '\n';
	s[i++] = '\0';
}
