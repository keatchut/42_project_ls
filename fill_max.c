/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_max.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:39:43 by keatchut          #+#    #+#             */
/*   Updated: 2018/02/01 02:34:53 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	void	fill_lstr(t_btree *tree, t_max *len)
{
	int t;

	t = ft_strlen(tree->usr);
	if (t > len->usr)
		len->usr = t;
	t = ft_strlen(tree->group);
	if (t > len->grp)
		len->grp = t;
}

static	void	fill_lsize2(t_btree *tree, t_max *len)
{
	int		i;
	long	t;

	i = 0;
	t = tree->size;
	while (t > 9)
	{
		t = t / 10;
		i++;
	}
	if (len->size < i + 1)
		len->size = i + 1;
}

static	void	fill_lsize(t_btree *tree, t_max *len)
{
	int		i;
	long	t;

	i = 0;
	if (S_ISCHR(tree->mode) || S_ISBLK(tree->mode))
	{
		t = minor(tree->dev);
		while (t > 9)
		{
			t = t / 10;
			i++;
		}
		t = major(tree->dev);
		while (t > 9)
		{
			t = t / 10;
			i++;
		}
		if (len->size < i + 6)
			len->size = i + 6;
	}
	else
		fill_lsize2(tree, len);
}

static	void	fill_llnk(t_btree *tree, t_max *len)
{
	int		i;
	long	t;

	i = 0;
	t = tree->nlinks;
	while (t > 9)
	{
		t = t / 10;
		i++;
	}
	if (len->lnk < i + 1)
		len->lnk = i + 1;
	i = 0;
	t = tree->size;
	while (t > 9)
	{
		t = t / 10;
		i++;
	}
}

void			fill_len(t_btree *tree, t_max *len)
{
	acl_t	acl;
	ssize_t	xattr;

	fill_llnk(tree, len);
	fill_lsize(tree, len);
	fill_lstr(tree, len);
	if ((xattr = listxattr(tree->path, NULL, 0, XATTR_NOFOLLOW)) > 0)
		tree->attr = '@';
	else if ((acl = acl_get_link_np(tree->path, ACL_TYPE_EXTENDED)))
	{
		acl_free((void *)acl);
		tree->attr = '+';
	}
	else
		tree->attr = ' ';
}
