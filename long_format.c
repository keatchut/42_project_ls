/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_format.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:41:12 by keatchut          #+#    #+#             */
/*   Updated: 2018/02/01 02:35:16 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			total_block(int total)
{
	char	s1[18];
	char	s2[10];
	int		t;
	int		i;
	int		j;

	j = 0;
	i = 0;
	t = total;
	ft_strcpy(s1, "total ");
	while (t > 9)
	{
		t = t / 10;
		i++;
	}
	j = i + 1;
	t = total;
	while (t > 9)
	{
		s2[i--] = t % 10 + '0';
		t = t / 10;
	}
	s2[i--] = t + '0';
	s2[j] = '\0';
	write(1, s1, ft_strlcat(ft_strcat(s1, s2), "\n", 18));
}

static char		filltype(mode_t mode)
{
	if (S_ISFIFO(mode))
		return ('p');
	else if (S_ISCHR(mode))
		return ('c');
	else if (S_ISDIR(mode))
		return ('d');
	else if (S_ISBLK(mode))
		return ('b');
	else if (S_ISLNK(mode))
		return ('l');
	else if (S_ISSOCK(mode))
		return ('s');
	return ('-');
}

static char		rights(mode_t mode, int i)
{
	if (((S_IRUSR & mode) && i == 1) || ((S_IRGRP & mode)
				&& i == 4) || ((S_IROTH & mode) && i == 7))
		return ('r');
	else if (((S_IWUSR & mode) && i == 2) || ((S_IWGRP & mode)
				&& i == 5) || ((S_IWOTH & mode) && i == 8))
		return ('w');
	else if (((S_IXGRP & mode) && i == 6 && (S_ISGID & mode))
			|| ((S_IXUSR & mode) && i == 3 && (S_ISUID & mode)))
		return ('s');
	else if ((i == 6 && (S_ISGID & mode)) || (i == 3 && (S_ISUID & mode)))
		return ('S');
	else if (i == 9 && (S_ISVTX & mode) && (S_IXOTH & mode))
		return ('t');
	else if (i == 9 && (S_ISVTX & mode))
		return ('T');
	else if (((S_IXUSR & mode) && i == 3) || ((S_IXGRP & mode)
				&& i == 6) || ((S_IXOTH & mode) && i == 9))
		return ('x');
	return ('-');
}

static	void	ivalue(t_opt *opt, int *i)
{
	if (opt->g == 0 && opt->o == 0)
		*i += opt->len->usr + opt->len->grp + 2;
	else if (opt->g == 1 && opt->o == 0)
		*i += opt->len->grp;
	else if (opt->g == 0 && opt->o == 1)
		*i += opt->len->usr;
}

void			build_lformat(t_btree *tree, t_opt *opt, int i, int a)
{
	char	s1[2 * PATH_MAX + 85];
	char	s2[2 * PATH_MAX + 3];

	s1[0] = filltype(tree->mode);
	while (i < 10)
	{
		s1[i] = rights(tree->mode, i);
		i++;
	}
	s1[i++] = tree->attr;
	a = i;
	i = 11 + opt->len->lnk;
	filllinks(s1, i, tree->nlinks, a);
	i += 2;
	fillgroup(s1, i, tree, opt);
	ivalue(opt, &i);
	a = i;
	i += opt->len->size + 1;
	fillsize(s1, i, tree, a);
	filldate(s1, i + 2, tree, opt);
	fillname(s2, tree, opt);
	write(1, s1, ft_strlcat(s1, s2, 2 * PATH_MAX + 85));
}
