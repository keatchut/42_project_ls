/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:41:37 by keatchut          #+#    #+#             */
/*   Updated: 2018/02/07 00:04:02 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			errno_message(char *name)
{
	char	s1[PATH_MAX + 60];
	int		i;
	int		a;

	i = 0;
	a = 0;
	ft_strcpy(s1, "ft_ls: ");
	while (s1[i] != '\0')
		i++;
	while (name[a] != '\0')
		s1[i++] = name[a++];
	a = 0;
	s1[i++] = ':';
	s1[i++] = ' ';
	s1[i++] = '\0';
	write(2, s1, ft_strlcat(ft_strcat(s1, strerror(errno)),
	"\n", PATH_MAX + 60));
}

static void		error_message(char c)
{
	char	s1[66];
	char	s2[40];
	int		i;

	ft_strcpy(s1, "ft_ls: illegal option -- ");
	ft_strcpy(s2, "\nusage: ft_ls [-GRSaglortu] [file ...]\n");
	i = 0;
	while (s1[i] != '\0')
		i++;
	s1[i] = c;
	s1[i + 1] = '\0';
	write(2, s1, ft_strlcat(s1, s2, 66));
}

static	void	display(int ac, char **av, t_opt *opt)
{
	int		i;

	i = 1;
	opt->rec = 0;
	get_opt(av, opt, &i, 0);
	if (opt->invalid != '\0')
	{
		error_message(opt->invalid);
		return ;
	}
	if (!av[i])
		cof_tree(".", 0, opt);
	else
	{
		while (i < ac)
		{
			cof_tree(av[i], 0, opt);
			i++;
		}
	}
	find(cof_tree(0, 1, opt), 1, opt);
	find(cof_tree(0, 0, opt), 0, opt);
	find(cof_tree(0, 3, opt), 3, opt);
}

static	int		init_len(t_opt *opt)
{
	t_max *len;

	if (!(len = (t_max *)malloc(sizeof(t_max))))
		return (0);
	len->lnk = 0;
	len->usr = 0;
	len->grp = 0;
	len->size = 0;
	opt->len = len;
	return (1);
}

int				main(int ac, char **av)
{
	t_opt *opt;

	if (!(opt = (t_opt *)malloc(sizeof(t_opt))))
		return (0);
	opt->ss = 0;
	opt->rr = 0;
	opt->a = 0;
	opt->l = 0;
	opt->r = 0;
	opt->t = 0;
	opt->gg = 0;
	opt->g = 0;
	opt->o = 0;
	opt->u = 0;
	opt->rec = 0;
	opt->invalid = '\0';
	if (!init_len(opt))
		return (0);
	if (ac > 1)
		display(ac, av, opt);
	else
		find_fold_elem(".", opt);
	free(opt->len);
	free(opt);
	return (0);
}
