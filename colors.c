/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:24:29 by keatchut          #+#    #+#             */
/*   Updated: 2018/01/31 03:24:37 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	char	*colornameb(t_btree *tree)
{
	char	s[NAME_MAX + 19];

	if (S_ISBLK(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[34;46m")
						, tree->name), "\033[0m"));
	else if (S_ISCHR(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[34;43m")
						, tree->name), "\033[0m"));
	else if (S_ISFIFO(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[33m")
						, tree->name), "\033[0m"));
	else if ((S_IXUSR & tree->mode) && (S_ISUID & tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[30;41m")
						, tree->name), "\033[0m"));
	else if ((S_IXUSR & tree->mode) && (S_ISGID & tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[30;46m")
						, tree->name), "\033[0m"));
	else if (S_IXUSR & tree->mode)
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[31m")
						, tree->name), "\033[0m"));
	else
		return (tree->name);
}

char			*colorname(t_btree *tree)
{
	char	s[NAME_MAX + 19];

	if ((S_ISVTX & tree->mode) && (S_IWOTH & tree->mode) && S_ISDIR(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[30;42m")
						, tree->name), "\033[0m"));
	else if ((S_IWOTH & tree->mode) && S_ISDIR(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[30;43m")
						, tree->name), "\033[0m"));
	else if (S_ISDIR(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[34m")
						, tree->name), "\033[0m"));
	else if (S_ISLNK(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[35m")
						, tree->name), "\033[0m"));
	else if (S_ISSOCK(tree->mode))
		return (ft_strcat(ft_strcat(ft_strcpy(s, "\033[32m")
						, tree->name), "\033[0m"));
	else
		return (colornameb(tree));
}
