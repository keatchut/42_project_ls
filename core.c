/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:24:53 by keatchut          #+#    #+#             */
/*   Updated: 2018/01/31 22:15:38 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static	void	avcase(t_btree *root, t_opt *opt, int *c)
{
	(*c)++;
	if (opt->gg == 1 && opt->l == 0)
		ft_strcpy(root->name, colorname(root));
	if (opt->l == 0 && opt->gg == 0)
	{
		ft_putstr(root->name);
		write(1, "\n", 1);
	}
	else
		build_lformat(root, opt, 1, 0);
}

static	void	dircase(t_btree *root, t_opt *opt, int *c)
{
	char display_path[PATH_MAX + 2];

	if (opt->rr == 0)
	{
		if ((*c) != 0 || opt->rec > 1)
			((*c) != 0) ? write(1, display_path, ft_strlcat(
						ft_strcat(ft_strcpy(display_path,
			"\n"), root->path), ":\n", PATH_MAX + 2)) :
				write(1, display_path, ft_strlcat(ft_strcpy(display_path,
				root->path), ":\n", PATH_MAX + 2));
		(*c)++;
		find_fold_elem(root->path, opt);
	}
	else
	{
		if ((*c) != 0 || opt->rec > 1)
			((*c) != 0) ? write(1, display_path, ft_strlcat(
						ft_strcat(ft_strcpy(display_path,
			"\n"), root->path), ":\n", PATH_MAX + 2)) :
				write(1, display_path, ft_strlcat(ft_strcpy(display_path,
				root->path), ":\n", PATH_MAX + 2));
		find_fold_elem(root->path, opt);
		(*c)++;
		find_fold_onl(root->path, opt);
	}
}

void			find(t_btree *root, int f, t_opt *opt)
{
	char		s[PATH_MAX + 34];
	static	int c;
	struct stat	st;

	if (root == NULL)
		return ;
	ft_strcpy(s, "ft_ls: ");
	!opt->r ? find(root->left, f, opt)
		: find(root->right, f, opt);
	if (f == 1)
	{
		lstat(root->name, &st);
		errno_message(root->name);
	}
	else if (f == 3)
		dircase(root, opt, &c);
	else
		avcase(root, opt, &c);
	!opt->r ? find(root->right, f, opt)
		: find(root->left, f, opt);
	free(root);
}
