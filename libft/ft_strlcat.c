/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 19:22:03 by keatchut          #+#    #+#             */
/*   Updated: 2017/11/10 16:16:01 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i;
	size_t k;
	size_t j;

	i = 0;
	k = 0;
	j = ft_strlen(src);
	while (dst[i] != '\0' && i < size)
		i++;
	if (size > 0)
	{
		while (src[k] != '\0' && i < size - 1)
		{
			dst[i] = src[k];
			i++;
			k++;
		}
	}
	if (k > 0)
	{
		dst[i] = '\0';
		return (j + (i - k));
	}
	return (i + j);
}
