# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: keatchut <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/31 03:42:24 by keatchut          #+#    #+#              #
#    Updated: 2018/02/20 13:38:20 by keatchut         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	ft_ls

SRCS	=	fill_max.c \
			fill.c \
			create_tree.c \
			sort_tree.c \
			core.c \
			colors.c \
			long_format.c \
			long_format2.c \
			parsing.c \
			repositories.c \
			main.c 

OBJS	=	$(SRCS:.c=.o)

HEADERS	=	ft_ls.h

LIB		=	libft/libft.a

CC		=	gcc

CFLAGS	=	-Wall -Wextra -Werror

all		:	$(NAME)

$(NAME)	:	$(OBJS) $(LIB)
			$(CC) $(CFLAGS) $(OBJS) -o $(NAME) $(LIB)

%.o		:	$(SPATH)/%.c
			$(CC) $(CFLAGS) -c $^ -o $@

$(LIB)	:	
			make -C libft

clean	:
			rm -f $(OBJS)
			make clean -C libft

fclean	:	clean
			rm -f $(NAME)
			make fclean -C libft

re		:	fclean all
