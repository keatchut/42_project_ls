/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_tree.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keatchut <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 03:37:12 by keatchut          #+#    #+#             */
/*   Updated: 2018/01/31 03:42:10 by keatchut         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	apply_infix(t_btree *root, t_opt *opt)
{
	if (root == 0)
		return ;
	apply_infix(root->left, opt);
	if (opt->gg == 1 && opt->l == 0)
		ft_strcpy(root->name, colorname(root));
	if (opt->l == 0)
	{
		if (root->stat_check == 1 || (opt->rr == 0
			&& opt->gg == 0 && opt->t == 0 && opt->ss == 0))
		{
			ft_putstr(root->name);
			write(1, "\n", 1);
		}
	}
	else
	{
		if (root->stat_check == 1)
			build_lformat(root, opt, 1, 0);
	}
	apply_infix(root->right, opt);
	free(root);
}

void	apply_outfix(t_btree *root, t_opt *opt)
{
	if (root == 0)
		return ;
	apply_outfix(root->right, opt);
	if (opt->gg == 1 && opt->l == 0)
		ft_strcpy(root->name, colorname(root));
	if (opt->l == 0)
	{
		if (root->stat_check == 1 || (opt->rr == 0
			&& opt->gg == 0 && opt->t == 0 && opt->ss == 0))
		{
			ft_putstr(root->name);
			write(1, "\n", 1);
		}
	}
	else
	{
		if (root->stat_check == 1)
			build_lformat(root, opt, 1, 0);
	}
	apply_outfix(root->left, opt);
	free(root);
}

int		sortbyatime(const char *av1, const char *av2)
{
	struct stat s1;
	struct stat s2;
	int			cmp;

	lstat(av1, &s1);
	lstat(av2, &s2);
	cmp = s2.st_atimespec.tv_sec - s1.st_atimespec.tv_sec;
	if (cmp == 0)
		cmp = ft_strcmp((const char *)av1, (const char *)av2);
	return (cmp);
}

int		sortbymtime(const char *av1, const char *av2)
{
	struct stat s1;
	struct stat s2;
	int			cmp;

	lstat(av1, &s1);
	lstat(av2, &s2);
	cmp = s2.st_mtimespec.tv_sec - s1.st_mtimespec.tv_sec;
	if (cmp == 0)
		cmp = ft_strcmp((const char *)av1, (const char *)av2);
	return (cmp);
}

int		sortbysize(const char *av1, const char *av2)
{
	struct stat s1;
	struct stat s2;
	int			cmp;

	lstat(av1, &s1);
	lstat(av2, &s2);
	cmp = s2.st_size - s1.st_size;
	if (cmp == 0)
		cmp = ft_strcmp(av1, av2);
	return (cmp);
}
